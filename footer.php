<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package woostify
 */

do_action( 'woostify_theme_footer' );
wp_footer();
?>

</body>
</html>
